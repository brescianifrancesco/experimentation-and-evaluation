import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class ExperimentRunner {

    private static Integer[] getSortedInts(int dimension) {
        Integer[] arr = new Integer[dimension];
        for (int i = 0; i < dimension; i++) {
            arr[i] = i;
        }
        return arr;
    }

    private static Double[] getSortedDoubles(int dimension) {
        Double[] arr = new Double[dimension];
        for (int i = 0; i < dimension; i++) {
            arr[i] = new Double(i);
        }
        return arr;
    }

    private static Integer[] getReverseSortedInts(int dimension) {
        Integer[] arr = new Integer[dimension];
        for (int i = dimension; i > 0; i--) {
            arr[dimension - i] = i;
        }
        return arr;
    }

    private static Double[] getReverseSortedDoubles(int dimension) {
        Double[] arr = new Double[dimension];
        for (int i = dimension; i > 0; i--) {
            arr[dimension - i] = new Double(i);
        }
        return arr;
    }

    private static Integer[] getRandomInts(int dimension) {
        Integer[] arr = new Integer[dimension];
        Random r = new Random();
        for (int i = 0; i < dimension; i++) {
            arr[i] = r.nextInt();
        }
        return arr;
    }

    private static Double[] getRandomDoubles(int dimension) {
        Double[] arr = new Double[dimension];
        Random r = new Random();
        for (int i = 0; i < dimension; i++) {
            arr[i] = r.nextDouble();
        }
        return arr;
    }

    final static int[] DIMENSIONS = new int[]{2, 100, 10000};

    enum Classes {Double, Integer}

    enum InitialSortings {S, SR, RS, R, Random}

    private static List<Long> measure(Sorter sorter, Comparable[] arr) {
        List<Long> measures = new ArrayList<>();

        for (int i = 0; i < 5; i++) {

            Comparable[] arrCopy = new Comparable[arr.length];
            Comparable[] shuffledArr = new Comparable[arr.length];
            System.arraycopy(arr, 0, arrCopy, 0, arrCopy.length);
            System.arraycopy(arr, 0, shuffledArr, 0, shuffledArr.length);

//            sort a shuffled array of the same dimension of arr
            List<Comparable> shuffledList = Arrays.asList(shuffledArr);
            Collections.shuffle(shuffledList);
            shuffledList.toArray(shuffledArr);
            sorter.sort(shuffledArr);

            long startTime = System.nanoTime();
            sorter.sort(arrCopy);
            long endTime = System.nanoTime();
            measures.add(endTime - startTime);
        }

        return measures;
    }

    private static void logMeasure(Sorter sorter, int dimension, Classes c, InitialSortings initialSorting, PrintWriter printWriter) {

        Comparable[] values = null;
        switch (initialSorting) {
            case S:
                switch (c) {
                    case Integer:
                        values = getSortedInts(dimension);
                        break;
                    case Double:
                        values = getSortedDoubles(dimension);
                        break;
                }
                break;
            case SR:
                switch (c) {
                    case Integer:
                        List<Integer> integers = new ArrayList<>();
                        integers.addAll(new ArrayList<>(Arrays.asList(getSortedInts(dimension / 2))));
                        integers.addAll(new ArrayList<>(Arrays.asList(getReverseSortedInts(dimension / 2))));
                        values = integers.toArray(new Integer[integers.size()]);
                        break;
                    case Double:
                        List<Double> doubles = new ArrayList<>();
                        doubles.addAll(new ArrayList<>(Arrays.asList(getSortedDoubles(dimension / 2))));
                        doubles.addAll(new ArrayList<>(Arrays.asList(getReverseSortedDoubles(dimension / 2))));
                        values = doubles.toArray(new Double[doubles.size()]);
                        break;
                }
                break;
            case RS:
                switch (c) {
                    case Integer:
                        List<Integer> integers = new ArrayList<>();
                        integers.addAll(new ArrayList<>(Arrays.asList(getReverseSortedInts(dimension / 2))));
                        integers.addAll(new ArrayList<>(Arrays.asList(getSortedInts(dimension / 2))));
                        values = integers.toArray(new Integer[integers.size()]);
                        break;
                    case Double:
                        List<Double> doubles = new ArrayList<>();
                        doubles.addAll(new ArrayList<>(Arrays.asList(getReverseSortedDoubles(dimension / 2))));
                        doubles.addAll(new ArrayList<>(Arrays.asList(getSortedDoubles(dimension / 2))));
                        values = doubles.toArray(new Double[doubles.size()]);
                        break;
                }
                break;
            case R:
                switch (c) {
                    case Integer:
                        values = getReverseSortedInts(dimension);
                        break;
                    case Double:
                        values = getReverseSortedDoubles(dimension);
                        break;
                }
                break;
            case Random:
                switch (c) {
                    case Integer:
                        values = getRandomInts(dimension);
                        break;
                    case Double:
                        values = getRandomDoubles(dimension);
                        break;
                }
                break;
        }

        List<Long> measures = measure(sorter, values);
        Long avg = measures.stream().skip(1).mapToLong(Long::longValue).sum() / measures.size() - 1;
        String csvLine = String.join(",", sorter.getClass().getSimpleName(), c.name(), Integer.toString(values.length), initialSorting.name(), Long.toString(measures.get(0)), Long.toString(measures.get(1)), Long.toString(measures.get(2)), Long.toString(measures.get(3)), Long.toString(measures.get(4)), Long.toString(avg));
        printWriter.println(csvLine);
        System.out.println(csvLine);
    }

    private static void runExperiments() throws IOException {


        for (int dimension : DIMENSIONS) {
            PrintWriter printWriter = new PrintWriter(new FileWriter(String.join("", "experiment-measures-", String.valueOf(dimension), ".csv")));

            String headers = String.join(",", "Algorithm", "Data Type", "Array Dimension", "Initial Sorting", "Measure1", "Measure2", "Measure3", "Measure4", "Measure5", "Avg Time last 4 runs (ns)");
            printWriter.println(headers);
            System.out.println("\n============================================\n");
            System.out.println(headers);
            System.out.println("\n============================================\n");
            for (Classes c : Classes.values()) {
                for (InitialSortings initialSorting : InitialSortings.values()) {
                    for (Sorter sorter : new Sorter[]{new BubbleSortPassPerItem(), new BubbleSortUntilNoChange(), new BubbleSortWhileNeeded()})
                        logMeasure(sorter, dimension, c, initialSorting, printWriter);
                }
            }
            printWriter.close();
        }


    }

    private final static String getKeyFor(String algorithm, String dimension) {
        return String.join("-", algorithm, dimension);
    }

    private static void computeAverages() throws IOException {



        for (int dimension : DIMENSIONS) {
            String fileToRead = String.join("","averages-",String.valueOf(dimension),".csv");
            String fileToWrite = String.join("","experiment-measures-",String.valueOf(dimension),".csv");
            PrintWriter printWriter = new PrintWriter(new FileWriter(fileToRead));

            String headers = String.join(",", "Algorithm", "Avg Time (ns)");
            printWriter.println(headers);
            System.out.println("\n============================================\n");
            System.out.println(headers);
            System.out.println("\n============================================\n");

            CSVReader reader = new CSVReader(new FileReader(fileToWrite), ',', '\'', 1);
            List<String[]> r = reader.readAll();
            Map<String, List<String>> measures = new HashMap();
            r.stream().forEach(el -> {
                if (measures.containsKey(getKeyFor(el[0], el[1]))) {
                    measures.get(getKeyFor(el[0], el[1])).add(el[9]);
                } else {
                    measures.put(getKeyFor(el[0], el[1]), new ArrayList<>(Arrays.asList(el[9])));
                }
            });
            measures.keySet().forEach(key -> {
                Long avg = measures.get(key).stream().skip(1).mapToLong(Long::new).sum() / measures.size() - 1;
                printWriter.println(String.join(": ", key, avg.toString()));
                System.out.println(String.join(": ", key, avg.toString()));

            });

            printWriter.close();
        }
    }

    public static void main(String[] args) throws IOException {

        runExperiments();
        System.out.println("\n====================================\n");
        computeAverages();


    }


}

